﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FxProperties.Models
{
    public class PropertyModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name="Property Name")]
        public string Name { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        public string State { get; set; }

        [Required]
        public int Supervisor { get; set; }

        [Required]
        public int LeaseAgent { get; set; }

        public int Buildings { get; set; }
        public int Units { get; set; }
        public int Occupied { get; set; }
        public int Vacant { get; set; }
        public int VacancyPercentage { get; set; }
    }

    public enum State
    {
        California,
        Texas
    }

    public class PropertyConfig
    {
        public int Buildings { get; set; }
        public int UnitFrom { get; set; }
        public int UnitTo { get; set; }
    }
}