﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FxProperties.Models
{
    public class UnitModel
    {
        public int Id { get; set; }
        public int UnitNo { get; set; }
        public int Type { get; set; }
        public decimal Rent { get; set; }
        public float Area { get; set; }
    }
}