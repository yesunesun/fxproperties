﻿using System;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Security.Claims;
using System.Security.Principal;
using FxProperties.Models;

namespace FxProperties.Code
{
    public static class ApplicationUser
    {
        public static string GetUserName(this IIdentity identity)
        {
            string title = string.Empty;
            using (var db = new UsersContext())
            {
                var user = db.UserProfiles.FirstOrDefault(u => u.UserName.ToLower() == identity.Name.ToLower());
                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

                if (user != null)
                    title = user.FirstName + " " + user.LastName;
                title = textInfo.ToTitleCase(title);
            }

            return title;
        }

        

        public static string GetUserRole(this IIdentity identity)
        {
            string role = string.Empty;
            using (var db = new UsersContext())
            {
                var user = db.UserProfiles.FirstOrDefault(u => u.UserName.ToLower() == identity.Name.ToLower());
                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

                if (user != null)
                    role = user.Role;
            }

            return role;
        }
    }
}