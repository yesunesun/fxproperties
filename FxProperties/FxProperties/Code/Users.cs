using System.Collections.Generic;
using System.Linq;
using FxProperties.Models;

namespace FxProperties.Code
{
    public class Users
    {
        public static List<UserModel> GetSupervisors()
        {
            var users = GetUsersByRole("supervisor");

            return users;
        }

        public static List<UserModel> GetLeasingAgents()
        {
            var users = GetUsersByRole("lagent");

            return users;
        }

        private static List<UserModel> GetUsersByRole(string role)
        {
            var users = new List<UserModel>();
            using (var db = new UsersContext())
            {
                var supervisors = db.UserProfiles.Where(u => u.Role.ToLower() == role);
                users.AddRange(supervisors.Select(user => new UserModel()
                {
                    FullName = user.FirstName + " " + user.LastName,
                    UserId = user.UserId,
                    Role = user.Role
                }));
            }
            return users;
        }
    }
}