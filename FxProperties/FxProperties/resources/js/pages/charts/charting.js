// Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawPieChart);
      google.charts.setOnLoadCallback(drawRentChart);
      google.charts.setOnLoadCallback(drawNonPerformingPieChart);


      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawRentChart() {

        // Create the data table.
        var data = google.visualization.arrayToDataTable([
        ["Day", "Collection" ],
        ["1", 9100],
        ["2", 1500],
        ["3", 23025],
        ["4", 8295],
        ["5", 14750],
        ["6", 12650],
        ["7", 9720],
        ["8", 7750],
        ["9", 7750],
        ["10", 2380],
        ["11", 4534],
        ["12", 9010],
        ["13", 0],
        ["14", 0],
        ["15", 0],
        ["16", 0],
        ["17", 0],
        ["18", 0],
        ["19", 0],
        ["20", 0],
        ["21", 0],
        ["22", 0],
        ["23", 0],
        ["24", 0],
        ["25", 0],
        ["26", 0],
        ["27", 0],
        ["28", 0],
        ["29", 0],
        ["30", 0]
      ]);

        // Set chart options
        var options = {'title':'Rent collection for the month of June',
                       'height':300};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('rent_div'));
        chart.draw(data, options);
      }

      function drawPieChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['Vacant', 30],
          ['Occupied', 522],
          ['Modal', 5],
          ['Employ', 20],
          ['Housing', 50],
          ['Maintenance', 15],
          ['Leased', 8]
        ]);

        // Set chart options
        var options = {'title':'Occupancy Chart', is3D: true, 'height':400};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }

      function drawNonPerformingPieChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['Vacant', 30],
          ['Evict', 2],
          ['Modal', 5],
          ['Employ', 20],
          ['Maintenance', 15]
        ]);

        // Set chart options
        var options = {'title':'Non Performing Units', is3D: true, 'height':400};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('nonperforming_chart_div'));
        chart.draw(data, options);
      }
