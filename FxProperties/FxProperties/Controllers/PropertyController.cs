﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using DotNetOpenAuth.Messaging;
using FxProperties.Models;

namespace FxProperties.Code
{
    //[Authorize]
    public class PropertyController : Controller
    {
        //
        // GET: /Property/

        public ActionResult Index()
        {
            var propNames = new List<string>() {
                "Fx of Point West",
                "Pelican's Landing Condominiums",
                "Miami Gardens",
                "Fx of Birch Brook Apartments",
                "Fx of West Park Apartments",
                "Fx of Dairy Ashford Apartments",
                "Fx of Kirkwood Apartments",
                "Fx of Town Park Apartments",
                "Fx of Deer Park Apartments",
                "Fx of Beechnut Apartments",
                "Fx of Edgebrook Apartments",
                "Fx of West Oaks",
                "Villagekey Apartments"
            };

            var props = new List<PropertyModel>();

            foreach (var propName in propNames)
            {
                var prop = new PropertyModel();
                prop.Name = propName;
                prop.Address = "245 E 20th St, New York, NY 201609";
                prop.Buildings = 10;
                prop.Units = 50;
                prop.Supervisor = 1;
                prop.LeaseAgent = 1;
                prop.Occupied = 40;
                prop.Vacant = 10;
                prop.VacancyPercentage = 20;

                props.Add(prop);
            }


            var db = new UsersContext();
            var fname = db.UserProfiles.First(x => x.UserName == User.Identity.Name);
            return View(props);
        }

        public ActionResult List()
        {
            var propNames = new List<string>() {
                "Fx of Point West",
                "Pelican's Landing Condominiums",
                "Miami Gardens",
                "Fx of Birch Brook Apartments",
                "Fx of West Park Apartments",
                "Fx of Dairy Ashford Apartments",
                "Fx of Kirkwood Apartments",
                "Fx of Town Park Apartments",
                "Fx of Deer Park Apartments",
                "Fx of Beechnut Apartments",
                "Fx of Edgebrook Apartments",
                "Fx of West Oaks",
                "Villagekey Apartments"
            };

            var props = new List<PropertyModel>();

            foreach (var propName in propNames)
            {
                var prop = new PropertyModel();
                prop.Name = propName;
                prop.Address = "245 E 20th St, New York, NY 201609";
                prop.Buildings = 10;
                prop.Units = 50;
                prop.Supervisor = 1;
                prop.LeaseAgent = 1;
                prop.Occupied = 40;
                prop.Vacant = 10;
                prop.VacancyPercentage = 20;

                props.Add(prop);
            }


            var db = new UsersContext();
            //var fname = db.UserProfiles.First(x => x.UserName == User.Identity.Name);
            return View(props);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PropertyModel model)
        {
            if (ModelState.IsValid)
            {
                using (var ctx = new FxPropertiesEntities())
                {
                    ctx.Properties.Add(new Property()
                    {
                        Name = model.Name,
                        Address = model.Address,
                        City = model.City,
                        LeasingAgent = model.LeaseAgent,
                        Supervisor = model.Supervisor,
                        State = model.State
                    });

                    ctx.SaveChanges();
                }
            }

            return View(model);
        }

        public ActionResult Create()
        {
            var prop = new PropertyModel();
            return View(prop);
        }

        public ActionResult Config()
        {
            return View();
        }

        public ActionResult Test()
        {
            var unitList = new List<UnitModel>();
            using (var ctx = new FxPropertiesEntities())
            {
                var units = ctx.Units.Where(u => u.Building.PropertyId == 1 && u.Status == 1).ToList();

                unitList.AddRange(units.Select(unit => new UnitModel()
                {
                    Id = unit.Id, 
                    Type = unit.Type ?? default(int), 
                    Area = unit.Area ?? default(float), 
                    Rent = unit.Rent ?? default(decimal)
                }));
            }

            return View(unitList);
        }
    }
}
