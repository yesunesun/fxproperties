//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FxProperties
{
    using System;
    using System.Collections.Generic;
    
    public partial class Building
    {
        public Building()
        {
            this.Units = new HashSet<Unit>();
        }
    
        public int Id { get; set; }
        public int BuildingNo { get; set; }
        public int PropertyId { get; set; }
        public int Floors { get; set; }
    
        public virtual Property Property { get; set; }
        public virtual ICollection<Unit> Units { get; set; }
    }
}
